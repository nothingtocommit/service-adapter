package com.gpncup.serviceadapter;

import com.gpncup.serviceadapter.soap.Calculator;
import com.gpncup.serviceadapter.soap.CalculatorSoap;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.event.annotation.BeforeTestClass;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.net.URL;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@EnableCaching
class ServiceAdapterApplicationTests {
	@Autowired
	private CalcService calcService;
	@MockBean
	private CalculatorSoap calcSoapService;
//	private static CalculatorSoap calculatorService;

//	@AfterEach
//	public void tearDown() {
//		Mockito.reset(calcSoapService);
//	}
//
	@Test
	public void add() {
		Mockito.doReturn(2).when(calcSoapService).add(1, 1);
		Assertions.assertEquals(2, calcService.add(1, 1));
	}

	@Test
	public void subtract() {
		Mockito.doReturn(1).when(calcSoapService).subtract(2, 1);
		Assertions.assertEquals(1, calcService.subtract(2, 1));
	}

	@Test
	public void multiply() {
		Mockito.doReturn(10).when(calcSoapService).multiply(2, 5);
		Assertions.assertEquals(10, calcService.multiply(2, 5));
	}

	@Test
	public void divide() {
		Mockito.doReturn(9).when(calcSoapService).divide(81, 9);
		Assertions.assertEquals(9, calcService.divide(81, 9));
	}

	@Test
	public void cache() {
		Mockito.doReturn(4).when(calcSoapService).add(2, 2);

		int addOperationCacheMiss = calcService.add(2, 2);
		int addOperationCacheHit = calcService.add(2, 2);

		Assertions.assertEquals(4, addOperationCacheMiss);
		Assertions.assertEquals(4, addOperationCacheHit);

		Mockito.verify(calcSoapService, Mockito.times(1)).add(2, 2);
	}

}
