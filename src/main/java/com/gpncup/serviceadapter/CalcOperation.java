package com.gpncup.serviceadapter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CalcOperation {
    enum CalcOperationTypes {
        ADD,
        SUBTRACT,
        MULTIPLY,
        DIVIDE
    }

    @NotNull
    CalcOperationTypes operationType;
    @NotNull
    Integer intA;
    @NotNull
    Integer intB;

    CalcOperation(String operationType, Integer intA, Integer intB) {
        this.operationType = CalcOperationTypes.valueOf(operationType.toUpperCase());
        this.intA = intA;
        this.intB = intB;
    }

    public CalcOperationTypes getOperationType() {
        return operationType;
    }

    public int getIntA() {
        return intA;
    }

    public int getIntB() {
        return intB;
    }

    public void setOperationType(String operationType) {
        this.operationType = CalcOperationTypes.valueOf(operationType.toUpperCase());
    }

    public void setIntA(int intA) {
        this.intA = intA;
    }

    public void setIntB(int intB) {
        this.intB = intB;
    }
}
