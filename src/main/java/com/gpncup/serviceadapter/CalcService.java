package com.gpncup.serviceadapter;

import com.gpncup.serviceadapter.soap.Calculator;
import com.gpncup.serviceadapter.soap.CalculatorSoap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class CalcService {
    @Autowired
    private CalculatorSoap calculatorService;

    @Cacheable(
        value="calculations",
        key="{#root.methodName, #number1, #number2}"
    )
    public int add(int number1, int number2) {
//        System.out.println(1);
        return calculatorService.add(number1, number2);
    }

    @Cacheable(
            value="calculations",
            key="{#root.methodName, #number1, #number2}"
    )
    public int subtract(int number1, int number2) {
        return calculatorService.subtract(number1, number2);
    }

    @Cacheable(
            value="calculations",
            key="{#root.methodName, #number1, #number2}"
    )
    public int multiply(int number1, int number2) {
        return calculatorService.multiply(number1, number2);
    }

    @Cacheable(
            value="calculations",
            key="{#root.methodName, #number1, #number2}"
    )
    public int divide(int number1, int number2) {
        return calculatorService.divide(number1, number2);
    }
}
