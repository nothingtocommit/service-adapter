package com.gpncup.serviceadapter;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.models.media.MapSchema;
import io.swagger.v3.oas.models.media.StringSchema;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Map;


@RestController
public class CalcController {
    @Autowired
    private CalcService calcService;

    @ApiResponses(value={
        @ApiResponse(responseCode = "200", description="Calculation performed",
            content={
                @Content(
                    mediaType="application/json"
                )
            }),
        @ApiResponse(responseCode = "400", description = "Not valid request body",
            content = {
                @Content(
                    mediaType="application/json"
                )
            })
    })
    @PostMapping("/calculator")
    public ResponseEntity<Map<String, Object>> calculate(
            @Valid @RequestBody CalcOperation calcOperation,
            BindingResult bindingResult
    ) {
        HttpStatus httpStatus;
        Map<String, Object> responseBody;
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getAllErrors());
            httpStatus = HttpStatus.BAD_REQUEST;
            responseBody = Map.of(
                    "error",
                    "Please provide: " +
                        "a valid name of the operation(ADD, SUBTRACT, MULTIPLY or DIVIDE) " +
                        "and TWO numbers as an input");
        } else {
            int result;
            switch (calcOperation.operationType) {
                case ADD:
                    result = calcService.add(calcOperation.intA, calcOperation.intB);
                    break;
                case SUBTRACT:
                   result = calcService.subtract(calcOperation.intA, calcOperation.intB);
                    break;
                case MULTIPLY:
                    result = calcService.multiply(calcOperation.intA, calcOperation.intB);
                    break;
                case DIVIDE:
                    result = calcService.divide(calcOperation.intA, calcOperation.intB);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + calcOperation.operationType);
            }
            responseBody = Map.of("result", result);
            httpStatus = HttpStatus.OK;
        }

        return new ResponseEntity<>(responseBody, httpStatus);
    }
}
