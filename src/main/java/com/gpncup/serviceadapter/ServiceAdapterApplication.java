package com.gpncup.serviceadapter;

import com.gpncup.serviceadapter.soap.Calculator;
import com.gpncup.serviceadapter.soap.CalculatorSoap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;

import java.time.Duration;

@SpringBootApplication
@EnableCaching
public class ServiceAdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceAdapterApplication.class, args);
	}

	@Bean
	RedisCacheConfiguration cacheConfiguration() {
		return RedisCacheConfiguration.defaultCacheConfig()
				.entryTtl(Duration.ofMinutes(2));
	}

	@Bean
	public CalculatorSoap calcSoapService() {
		return new Calculator().getCalculatorSoap();
	}
}
